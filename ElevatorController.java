public class ElevatorController {
    // ElevatorController class (Singleton and Command)
    private static final ElevatorController elevatorController = new ElevatorController();
    public static ElevatorController getInstance() {
        return elevatorController;
    }

    private ElevatorController() {
    }

    public void addElevator(Elevator elevator) {
        // Add elevators to control
    }

    public void assignElevator(int floor, int direction) {
        // Assign elevator to handle request
    }

    public void moveElevator(Elevator elevator, int floor) {
        // Move elevator to floor
    }
}
