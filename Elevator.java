import java.util.Observable;

public class Elevator extends Observable {
    // Elevator class (Singleton and Observer)
    // Handle elevator movement and stops


    public void move(int floor) {
        // Move the elevator and notify passengers
        setChanged();
        notifyObservers(floor);
    }
}
