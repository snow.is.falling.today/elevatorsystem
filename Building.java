
class Building {
    private static final Building building = new Building();
    public static Building getInstance() {
        return building;
    }

    private Building() {
    }

    // Building class (Singleton and Factory Method)
    // Attributes and methods

    public void createPassengers() {
        // Create passengers on each floor
    }
}
